# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include bluefish::install
class bluefish::install {
  package { $bluefish::package_name:
    ensure => $bluefish::package_ensure,
  }
}
