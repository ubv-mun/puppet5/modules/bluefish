# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include bluefish
class bluefish (
  String        $package_ensure,
  Array[String] $package_name,
) {
  contain 'bluefish::install'
}
